import React, { useState } from 'react';
import './App.css';
import * as firebase from 'firebase';

const Config = {
  apiKey: "AIzaSyB-5cWxgz6h0cRy2XOH6oKdMqaRrhPCJvU",
  authDomain: "awesome-project-af78d.firebaseapp.com",
  databaseURL: "https://awesome-project-af78d.firebaseio.com",
  projectId: "awesome-project-af78d",
  storageBucket: "awesome-project-af78d.appspot.com",
  messagingSenderId: "510142973395",
  appId: "1:510142973395:web:ba1a66732c78264fd09b26",
  measurementId: "G-D3QSYVK12J"
};

firebase.initializeApp(Config);
const auth = firebase.auth();

function App() {

  const [Lemail, setLogEmail] = useState("");
  const [Lpswd, setLogPswd] = useState("");
  const [Lfile, setLogFile] = useState("");
  const [FileLoader, setFileLoader] = useState("");
  const [Remail, setRegEmail] = useState("");
  const [Rpswd, setRegPswd] = useState("");
  const [Rfile, setRegFile] = useState("");
  const [Ffile, setFFile] = useState("");
  const [Status, setStatus] = useState("");
  const [Error, setError] = useState("");

  const handleNewData = (e) => {
  
    alert('A name was submitted: ');
    // e.preventDefault();
  
    // let users = {
    //   user1:{
    //     name: "Paco",
    //   },
    //   user2:{
    //     name: "Alfredo",
    //   }
    // }
    // const userRef = firebase.database().ref().push(users);
    // userRef.once('value', snapshot => {
    //   console.log(snapshot.val());
    // });
  }


  // componentDidMount(){
  //   const nameRef = firebase.database().ref().child('users').child('name');
  //   nameRef.on('value', snapshot => {
  //     this.setState({
  //       name: snapshot.val()
  //     })
  //     console.log( this.state.name );
  //   })
  // }
 
  const handleUploadFile = ( id ) => {
    let finalUrl;
    const storage = firebase.storage();
    const storageRef = firebase.storage().ref(`pictures/${id}/${Rfile.name}`);
    const task = storageRef.put(Rfile);
    task.on('state_changed', (snapshot) => {
      let percentage = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      setFileLoader( percentage );
    }, (error) => {
      setError(error.message);
    }, () => {
      // Upload complete
      return storage
        .ref( `/pictures/${id}/${Rfile.name}` )
        .getDownloadURL()
        .then( url => {
          console.log( "Got download url: ", url );
          setFFile(url);
          finalUrl = url;
          console.log(Ffile);
        });
    })
    return finalUrl
  }

  const handleSubmitReg = e => {
    e.preventDefault();
    auth.createUserWithEmailAndPassword( Remail, Rpswd )
      .then(() =>{
        var user= firebase.auth().currentUser;
        console.log( user.uid );
        const url = handleUploadFile( user.uid );
        console.log( Ffile );
        
        firebase.database().ref('/Users').child( user.uid ).update( { Email: Remail, ProfileImage: url } );

      })
      .catch(e => {
        setError(e.message);
      });
  }

  const handleSubmitLog = ( e ) => {
    e.preventDefault();
    console.log('click registro');
    // firebase.auth().signInWithEmailAndPassword( Lemail, Lpswd )
    //   .then(res => {
    //     var userId = firebase.auth().currentUser.uid;
    //     console.log(res.user.uid)
    //     return firebase.database().ref(`schools/`).child(userId).once('value').then(function(snapshot) {
    //       console.log(snapshot.val())
    //       setStatus(snapshot.val());
    //     });
    //   })
    //   .catch(e => {
    //     setError(e.message);
    //   });
  }

  return (
    <div className="App">
      <div className="App-header">
        <div>
          <form onSubmit={ e => handleSubmitLog(e) }>
            <h2>Login</h2>
            <input type={'text'} onChange={ e =>  setLogEmail(e.target.value)}/><br/>
            <input type={'text'} onChange={ e => setLogPswd(e.target.value)}/><br/>
            <button type={'submit'} >send</button>
          </form>
        </div>

        <div>
          <form onSubmit={ e => handleSubmitReg(e) }>
            <h2>Registro</h2>
            <input type={'text'} onChange={e => setRegEmail(e.target.value)}/><br/>
            <input type={'text'} onChange={e => setRegPswd(e.target.value)}/><br/>
            <input type="file" name="upload" onChange={ e => setRegFile(e.target.files[0])  /* this.handleUploadFile.bind( this ) */ } /><br/>
            {FileLoader}<br/>
            <button type={'submit'}  >send</button>
          </form>
        </div>
        <div>
          <h2>Status</h2>
          { Status } <br/> { Error } <br/>
          <img width='90' src={ Lfile } alt="img" /><br/>
          { Lemail }<br/>
        </div>
      </div>
    </div>
  );
}

export default App;
